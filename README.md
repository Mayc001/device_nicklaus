# device_nicklaus

Árvore de dispositivos para Motorola Moto E4 Plus
O Motorola Moto E4 Plus (codinome "nicklaus" ) é um smartphone de gama média da Motorola. Clique aqui para ver o manual de construção

Basic	Folha de especificações
CPU	Cortex-A53 de 1,3 GHz e quatro núcleos
Chipset	MediaTek MT6737M
GPU	Mali-T720 MP2
Memória	2GB RAM
Versão Android enviada	7.1
Armazenamento	16 / 32GB
MicroSD	Até 128 GB
Bateria	5000mAh
Exibição	720 x 1280 pixels, 5,5 polegadas
Câmera	Principal 13MP / frontal 5MP, foco automático, flash LED
Moto E4 Plus